---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-08-31

##### Country Specific Data

| *Country Information*                     | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Based in APAC                             | 58      | 7.07%         |
| Based in EMEA                             | 229     | 27.93%        |
| Based in LATAM                            | 15      | 1.83%         |
| Based in NA                               | 518     | 63.17%        |
| *Total Team Members*                      | *820*   | *100%*        |

##### Gender Data

| *Gender (All)*                            | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Men                                       | 601     | 73.29%        |
| Women                                     | 219     | 26.71%        |
| Other Gender Identities                   | 0       | 0%            |
| *Total Team Members*                      | *820*   | *100%*        |

| *Gender in Leadership*                    | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Men in Leadership                         | 36      | 75.00%        |
| Women in Leadership                       | 12      | 25.00%        |
| Other Gender Identities                   | 0       | 0%            |
| *Total Team Members*                      | *48*    | *100%*        |

| *Gender in Development*                   | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Men in Development                        | 311     | 83.38%        |
| Women in Development                      | 62      | 16.62%        |
| Other Gender Identities                   | 0       | 0%            |
| *Total Team Members*                      | *373*   | *100%*        |

##### Race/Ethnicity Data

| *Race/Ethnicity (US Only)*                | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 29      | 5.87%         |
| Black or African American                 | 15      | 3.04%         |
| Hispanic or Latino                        | 28      | 5.67%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 20      | 4.05%         |
| White                                     | 289     | 58.50%        |
| Unreported                                | 113     | 22.87%        |
| *Total Team Members*                      | *494*   | *100%*        |

| *Race/Ethnicity in Development (US Only)* | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 10      | 6.58%         |
| Black or African American                 | 4       | 2.63%         |
| Hispanic or Latino                        | 9       | 5.92%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 7       | 4.61%         |
| White                                     | 91      | 59.87%        |
| Unreported                                | 31      | 20.39%        |
| *Total Team Members*                      | *152*   | *100%*        |

| *Race/Ethnicity in Leadership (US Only)*  | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 5       | 13.16%        |
| Black or African American                 | 0       | 0.00%         |
| Hispanic or Latino                        | 0       | 0.00%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 1       | 2.63%         |
| White                                     | 22      | 57.89%        |
| Unreported                                | 10      | 26.32%        |
| *Total Team Members*                      | *38*    | *100%*        |

| *Race/Ethnicity (Global)*                 | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 60      | 7.32%         |
| Black or African American                 | 21      | 2.56%         |
| Hispanic or Latino                        | 42      | 5.12%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 27      | 3.29%         |
| White                                     | 452     | 55.12%        |
| Unreported                                | 218     | 26.59%        |
| *Total Team Members*                      | *820*   | *100%*        |

| *Race/Ethnicity in Development (Global)*  | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 31      | 8.31%         |
| Black or African American                 | 7       | 1.88%         |
| Hispanic or Latino                        | 21      | 5.63%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 12      | 3.22%         |
| White                                     | 205     | 54.96%        |
| Unreported                                | 97      | 26.01%        |
| *Total Team Members*                      | *373*   | *100%*        |

| *Race/Ethnicity in Leadership (Global)*   | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| Asian                                     | 5       | 10.42%        |
| Black or African American                 | 0       | 0.00%         |
| Hispanic or Latino                        | 1       | 2.08%         |
| Native Hawaiian or Other Pacific Islander | 0       | 0.00%         |
| Two or More Races                         | 1       | 2.08%         |
| White                                     | 26      | 54.17%        |
| Unreported                                | 15      | 31.25%        |
| *Total Team Members*                      | *48*    | *100%*        |

##### Age Distribution

| *Age Distribution (Global)*               | *Total* | *Percentages* |
|-------------------------------------------|---------|---------------|
| 18-24                                     | 15      | 1.83%         |
| 25-29                                     | 153     | 18.66%        |
| 30-34                                     | 237     | 28.90%        |
| 35-39                                     | 160     | 19.51%        |
| 40-49                                     | 171     | 20.85%        |
| 50-59                                     | 75      | 9.15%         |
| 60+                                       | 8       | 0.98%         |
| Unreported                                | 1       | 0.12%         |
| *Total Team Members*                      | *820*   | *100%*        |


Source: GitLab's HRIS, BambooHR

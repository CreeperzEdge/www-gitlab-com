---
layout: job_family_page
title: "Product Designer"
---

At GitLab, Product Designers collaborate closely with Product Managers, Engineers, UX Research, and other Product Designers to create a [productive, minimal, and human][pajamas] experience. Product Designers report to a UX Manager.

## Responsibilities

* Help to define and improve the interface and experience of GitLab.
* Design features that fit within the larger experience and flows.
* Create deliverables (wireframes, mockups, prototypes, flows, etc.) to communicate ideas.
* Work with Product Managers and Engineers to iterate on and refine the existing experience.
* Involve UX Researchers when needed, and help them define research initiatives (usability tests, surveys, interviews, etc.).
* Stay informed and share the latest on UI/UX techniques, tools, and patterns.
* Understand responsive design and best practices.
* Have working knowledge of HTML, CSS. Familiarity with Javascript.
* Have knowledge and understanding of design systems theory and practice.
* Have a general knowledge of Git flow (feature branching, etc.), merge/pull requests, pipelines, and code testing.

#### Product Designer

* Deeply understand the technology and features of the stage group to which you are assigned.
* Actively evaluate and incorporate feedback from UX Research, and conduct evaluative research with guidance from the UX Research Team and/or Senior Product Designers.
* Help create strategic deliverables like journey maps, storyboards, competitive analyses, and personas.
* Create well-designed tactical deliverables, like wireframes and prototypes, to communicate your ideas.										
* Proactively identify both small and large usability issues within your stage group.
* Participate in Design Reviews, giving and receiving feedback in an appropriate way.
* Actively contribute to the [Pajamas Design System][pajamas].
* Break down designs to fit within the monthly release cadence, engage your team to review designs early and often, and productively iterate across milestones. Prioritize your tasks, even when deadlines seem overwhelming, and ask for help, as needed.
* Participate in and help drive [product discovery](https://about.gitlab.com/handbook/product/#product-discovery-issues) to generate ideas for features and improvements.
* Take part in the monthly release process by reviewing and approving UX implementations.

#### Senior Product Designer

Everything in the Product Designer role, plus:

* Deeply understand the technology and features of the stage group to which you are assigned, and have working knowledge of the end-to-end GitLab product.
* Proactively identify strategic UX needs within your stage group, and engage other Product Designers within your stage group to help you create deliverables like journey maps, storyboards, competitive analyses, and personas.
* Proactively identify both small and large usability issues within your stage group, and help influence your Product Manager to prioritize them.
* Proactively identify user research needs, and conduct evaluative research with guidance from the UX Research team.
* Broadly communicate the results of UX activities within your stage group to the UX department, cross-functional partners within your stage group, and other interested GitLab team-members using clear language that simplifies complexity.
* Mentor other members of the UX department, both inside and outside of your stage group.
* Actively contribute to the [Pajamas Design System][pajamas].
* Engage in social media efforts, including writing blog articles and responding on Twitter, as appropriate.
* Interview potential UX candidates.

#### Staff Product Designer

Everything in the Senior Product Designer role, plus:

* Understand technology and features of the end-to-end GitLab product, so you can assist the department in viewing product design from a holistic and comprehensive perspective.
* Proactively identify strategic UX needs that have cross-product impact, and assist UX Managers with collaborating to address them. Create concept designs, as needed, to support these efforts.
* Lead UX strategy and execution for the [Pajamas Design System][pajamas]. Propose processes and collaborate with cross-functional leaders to get buy in and prioritization.
* Be a major contributor to designing, documenting, and building out UI components in the Pajamas Design System.
* Review UI designs from across all stage groups to help ensure functional consistency throughout the product.
* Identify process and workflow improvements that will help the UX department work better together and with cross-functional partners. Work with the UX Director to get buy in for these optimizations.
* Interview potential UX candidates.
* Continually connect and drive current work towards our [Product Vision](/direction/).

#### Visual Designer

* Lead UX efforts to bring visual refinement to the GitLab product.
* Partner with Product Designers and Researchers to conduct user testing, incorporate feedback, and share findings and recommendations.
* Create deliverables (lo-fi wireframes, hi-fi mockups, prototypes, etc.) to communicate ideas, as needed.
* Refine and extend the existing custom icon library, and create illustrations for things like null states, error pages, and supplemental illustrations on design.gitlab.com.
* Work with developers to iterate on and refine the UI experience. Implement designs in HTML/CSS, as needed.
* Stay informed of the latest UI/UX techniques, tools, and patterns in enterprise tool design.
* Review new designs created by Product Designers to help ensure consistency throughout the system.
* Create and maintain documentation for the [Pajamas Design System][pajamas].

## Tools

Tools used by the UX department are flexible depending on the needs of the work. Please see the [Product Designer Onboarding](/handbook/engineering/ux/uxdesigner-onboarding/) page for more details.

## Performance indicators
* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)
* [Ratio of Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)
* [Beautifying our UI](/handbook/engineering/ux/performance-indicators/#ui-beautification)
* [Ratio of Breadth vs Depth Work](/handbook/engineering/ux/performance-indicators/#ratio-of-breadth-vs-depth-work)
* [UX Debt](/handbook/engineering/ux/performance-indicators/#ux-debt)


## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [UX Team](/handbook/engineering/ux/)
- [GitLab Design Kit](https://gitlab.com/gitlab-org/gitlab-design)
- [GitLab Design System][pajamas]

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his/their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one our Global Recruiters. In this call we will discuss your experience, understand what you are looking for in a Product Design role, talk about your work and approach to product design, discuss your compensation expectations, reasons why you want to join GitLab and answer any questions you have.
* Next, if a candidate successfully passes the screening call, candidates will be invited to schedule a 45 minute first interview with a Product Designer. In this interview, we will want you to talk through a case study in your portfolio so that we can find out about your process and understand your approach to design, your philosophy on design, and understand what you're looking for generally in a Product Designer position. This interview will also cover the more technical elements of design, so be prepared to talk about the tools and skills you use as a Designer.
* If you successfully pass the previous stage, candidates will then be invited to schedule a 1-hour interview with a UX Manager. This interview will typically focus on assessing your research, strategy, and design skills. The interviewer will want to understand how you have incorporated research into your work and get a feel for your understanding of the fundamentals of research and UX methodology. Be prepared to answer questions around the soft skills Product Designers need, and be prepared to talk the interviewer through how you apply these skills in the real world.
* Candidates will be invited to schedule a third 45-minute interview with our UX Director if they successfully pass the previous interview. In this final interview, we will be looking for you to give some real insight into a problem you were solving as part of project you've worked on. We'll look to understand the size and structure of the team you were a part of, the goals of the project, your low-fidelity design work, your high-fidelity design skills, how you approached research, how you synthesised research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with a wider team.
* Finally, you will interview with a Product Manager who will focus on your ability to collaborate with Product teams and how your skills align with the needs of a specific stage group.
* Successful candidates will subsequently be made an offer via a video call or phone call.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[groups]: /company/team/structure/#groups
[pajamas]: https://design.gitlab.com

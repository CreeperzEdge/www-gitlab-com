---
layout: job_family_page
title: "Engineering Management - Infrastructure"
---

## Engineering Manager, Reliability

The Engineering Manager, Reliability directly manages the engineering team relentlessly focused on GitLab.com's
performant reliability, a team composed of [DBRE](/job-families/engineering/database-reliability-engineer/)s and
[SRE](/job-families/engineering/site-reliability-engineer/)s. They see the team as their product. They may work on
small features or bugs to keep their technical skills sharp and stay familiar with the code, but they emphasize
hiring a world-class team and putting them in the best position to succeed. They own the delivery of product
commitments and they are always looking to improve the productivity of their team. They must also coordinate
across departments to accomplish collaborative goals.

### Responsibilities
 - Hire an incredible team that lives our [values](/handbook/values/)
 - Improve the happiness and productivity of the team
 - Work on small features and bugs (nothing critical path)
 - Own Incident and Change Management, RCA, and Error Budget Management
 - Hold regular 1:1's with team members
 - Manage agile projects
 - Work across sections within engineering
 - Improve the quality, security and performance of the product

### Requirements
 - 2-5 years managing software engineering teams
 - Demonstrated teamwork in a peak performance organization
 - Experience running a consumer scale platform
 - Product company experience
 - Enterprise software company experience
 - Computer science education or equivalent experience
 - Passionate about open source and developer tools
 - Exquisite communication skills
 - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

### Nice-to-have's
 - Online community participation
 - Remote work experience
 - Startup experience
 - Significant open source contributions

### Performance Indicators

Engineering Manager, Infrastructure, have the following job-family performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
- [Infrastructure Cost vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)

## Senior Director of Infrastructure

The Senior Director of Engineering, Infrastructure manages multiple teams that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both SREs and managers. They can also grow the existing talent on their teams. Like all GitLab Engineering Directors, this role is a senior leader that models the behaviors we want to see in our teams and holds others accountable when necessary. And they create the collaborative and productive environment in which SREs and SRE managers do their work and succeed.

### Responsibilities

- Hire and manage multiple infrastructure teams that live our [values](/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Define the agile development and continuous delivery process
- Drive quarterly OKRs
- Work across departments within engineering
- Write public blog posts and speak at conferences
- Own the availability, reliability, and performance of GitLab.com
- Drive process for incident management
- Drive process for project management with transparent status and high productivity
- Plan ahead of required system capacity
- Drive infrastructure cost efficiency
- Communicate clearly and concisely with peers within Engineering and executives
- Create easy-to-understand SLOs and transparent dashboards

### Requirements

- 10 years managing multiple operations teams
- Excellent recruiter capable of attracting top talent
- Experience with consumer-level scale systems
- Passionate about open source and developer tools
- Experience in a peak performance organization
- Enterprise software company experience
- Product company experience
- Startup experience

### Nice-to-have's

- Candidates with diverse backgrounds
- Prior remote work experience
- Significant open source contributions
- Experience with global teams

### Performance Indicators

The Senior Director of Infrastructure, has the following performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
- [Infrastructure Cost vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)

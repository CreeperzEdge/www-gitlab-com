---
layout: job_family_page
title: "People Operations Specialist"
---
<a id="specialist-requirements"></a>

A junior team member in this role will be able to perform the majority of this role with a moderate level of guidance from the People Ops Director.  They will be more focused on execution of the above tasks rather than the strategy behind those decisions. However, it is the intention that this role will learn to make those independent choices and will determine ways to increase the efficiency of the tasks needed in this role.  Targeted areas of focus for the team member in this role will include:

### Responsibilities

 - Coordinate all offboarding tasks and provide assistance when needed.
 - Coordinate all onboarding tasks and provide assistance when needed.
 - Process all reference / visa / mortgage letter requests.
 - Responsible for providing support for company wide use of Zoom, Moo and NexTravel (permissions, troubleshooting).
 - Manage the Team Meetings calendar
 - Provide assistance in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
 - Work closely with People Operations and broader People team to implement specific People Operations processes and transactions.
 - Complete ad-hoc projects, reporting, and tasks.
 - Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
 - Assist with other People Operation activities as needed.

### Requirements

- The ability to work autonomously and to drive your own performance & development would be important in this role.
- Prior extensive experience in an HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work strange hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Strong team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values.
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelors degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.

## Levels

### Intermediate People Operations Specialist

- Support People Operations Specialists with onboarding tasks and ensure all onboarding is completed in a timely manner.
- Engage with co-employer’s and PEO”s on a regular basis, especially during onboarding within those regions. 
- Identify gaps during GitLab onboarding and co-employer processes.
- Identify automation opportunities anb work closely with the Manager, People Operations and Senior People Operations Specialist on what improvements are needed and why.
- Provide assistance in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Handle transfers for existing team members.
- Coordinate with Finance on Professional Employer Organisation's and Co-employer's payroll issues. 
- Administration of the signing of our Code of Conduct (annual signing in February and reporting on this).
- Administering anniversary gifts.
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Complete ad-hoc projects, reporting, and tasks.
- Administer probation period process - liaising with managers, communicating with PBP’s, updating team members.

### Senior People Operations Specialist

A Senior level team member in this role will be expected to be strategic and operational. They will need to identify key areas for improvement or scale and to suggest solutions. They will also be a mentor to other members on the People Operations and Recruiting teams. They will understand employment law and best practices and will be able to translate that knowledge into scalable solutions for GitLab.  Some key areas of focus for the Senior People Ops Specialist will include:

- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale.
- Foster co-employer relationships and act as the main point of contact.
- Work on onboarding people experience, values alignment during onboarding and improving onboarding through continuous iteration.
- Partner with Legal / Tax on international employment and contractual requirements.
- Provide assistance in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Handle WBSO grant with the Director of Tax.
- Review anniversary gift feedback on an annual basis and work with the supplier to iterate on the experience and ordering process.
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Complete ad-hoc projects, reporting, and tasks.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Update probation periods in BambooHR and the handbook. 
- Review new locations and work with the Professional Employer Organisations on converting team member's in these locations, while still ensuring a great people experience.

## Performance Indicators

Primary performance indicators for this role:

- [Onboarding TSAT > 4](/handbook/people-operations/people-operations-metrics/#onboarding-enps)

---
layout: job_family_page
title: "Reference Program Manager"
---

As a Customer Reference Manager, you will be part of a team responsible for managing and growing a compelling and persuasive customer reference program that provides timely references, nurtures a vibrant peer review community, and delivers impactful case studies in support of sales, analyst relations, field events, press releases and other marketing activities.

## Requirements

* Prior experience working with customers (references), influencers, and/or peer communities in B2B enterprise software and services required.
* Understanding of how to build relationships with volunteers in reference programs.
* Specific experience working with peer review sites and communities like G2, IT Central Station, and Gartner Peer Insights is a plus. 
* Excellent project and time management skills.
* Excellent written and verbal communications skills.
* Self-starter with a strong sense of ownership.
* Able to prioritize in a complex, fast-paced and lean organization.
* Passion for building a world class program and desire to own and refine key operational processes
* You share our [values](/handbook/values), and work in accordance with those values


## Responsibilities

* Help manage the GitLab worldwide customer reference program, working cross-functionally between sales, customer success and marketing to identify, engage, and develop reference customers.
* Manage high-touch customer reference relationships.
* Create and maintain in-depth customer profiles and contacts.
* Stimulate, nurture, and curate review activity on peer review sites and communities like G2, IT Central Station, and Gartner Peer Insights.
* Interface with sales to understand their needs and advise them on how/when to best use references.
* Support and provide input into marketing and sales deliverables including but not limited to:  peer to peer networking, event speaking opportunities, and 1: many reference calls.
* Manage reference requests tracking reference activity and outcomes.
* Identify and track potential customer success stories from the reference program and other sources.
* Create and help create customer success stories/case studies.
* Serve as a Program Manager for GitLab Customer Advisory Boards.
* Help define customer reference metrics/goals/benchmarks and track/report progress against them.
* Report outcome metrics for strategic customer engagement, and foster and promote long-term, mutually beneficial customer relationships.


## Senior Customer Reference Manager
As Customer Reference Managers progress throughout their career at GitLab or we find the need to hire a Customer Reference Manager who has more years of relevant experience, there will be a need for a Senior Customer Reference Manager position within the Market Research and Customer Insights team. The Senior Customer Reference Manager will report into the Manager, Market Research and Customer Insights.

### Requirements

This role includes all of the requirements above, plus:
* 5+ years of specific, significant experience in a Customer Reference Manager role in enterprise software.
* Proven ability to build and drive a budget and forecast spend.
* Specific experience in managing Customer Advisory Boards and/or similar executive-level programs.
* Strong orientation to managing program details.

### Responsibilities

This role includes all of the responsibilities above, plus:
* Be a leader in working cross-functionally to drive the execution of Customer Reference plans, aligning with other areas of marketing.
* Driving negotiations with supporting vendors and related firms to consistently optimize spend.
* Be willing to act as a Senior leader on the Market Research and Customer Insights team, mentoring and guiding Customer Reference Managers.
* Be a leader in building and driving process within the Market Research and Customer Insights team.

## Staff Customer Reference Manager
As Senior Customer Reference Managers progress throughout their career at GitLab or we find the need to hire a Senior Customer Reference Manager who has more years of relevant experience, there will be a need for a Staff Customer Reference Manager position in the Market Research and Customer Insights team. The Staff Customer Reference Manager will report into the Manager, Market Research and Customer Insights.

### Requirements

This role includes all of the requirements above, plus:
* Strong analytical skills and proven ability to use data to optimize program performance and inform future strategies.
* Plan and operate in a transparent manner for cross-organizational visibility and be a leader in sharing best practices with other Market Research and Customer Insights team members.
* Experience organizing executive customer events that both support key customers and provide insights back into all applicable business activities.
* Experience developing internal Customer Reference training programs and maintaining consistent internal and external communications of Customer Reference activities.
* Experience implementing and/or significantly utilizing an application to support ongoing customer reference program activities.

### Responsibilities

This role includes all of the responsibilities above, plus:
* 10+ years specific, significant experience in an industry Customer Reference role in enterprise software.
* Ability to easily transition from high level strategic thinking to creative and detailed execution.
* Excellent communicator with proven ability to clearly convey ideas and data in written and verbal presentations to a variety of audiences.
* Ability and interest to design, build, implement, and use data-driven mechanisms to simplify and speed decision making regarding Customer Reference activities.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Following that, candidates will be invited to schedule a 45 minute interview with a Regional Sales Director, US West, US East, or EMEA.
* Candidates will then be invited to schedule a 45 minute interview with a Reference Program Manager
* Next, candidates will be invited to schedule a 45 minute interview with a Senior Product Marketing Manager
* Candidates will then be invited to schedule a 45 minute interview with our Manager, Market Research and Customer Insights
* Then, Candidates will be invited to schedule a 45 minute interview with our Director of Product Marketing.
* Finally, selected candidates may be asked to interview with our CMO and/or CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

---
layout: markdown_page
title: Product Vision - Expansion
---

### Overview

The Expansion Team at GitLab focuses on running experiments to increase the expansion of our platform and expanding usage by teams within an organization or by individual users additionally we strive to increase the value of Gitlab to existing customers getting them to adopt new features in higher paid tiers. It's easy to look at GitLab and quickly come to a conclusion that our platform is for technical teams only (e.g. developers and engineers) but in fact many other non-technical teams can use GitLab to increase efficiency and integrate their work into a company's development life-cycle. To call out a few Product Managers, Data Analysts, Marketers, Support, UI/UX and Executives use GitLab in their day today. We want to get these teams to that ‘ah-Ha!’ moment so they can also benefit from using the platform. Our experiments are all public and we encourage feedback from the community and internal team. 

**Expansion Team**

Product Manager: [Tim Hey](https://about.gitlab.com/company/team/#_timhey) | Engineering Manager: [Bartek Marnane (Interim)](https://about.gitlab.com/company/team/#bartekmarnane) | UX Manager: [Jacki Bauer](https://about.gitlab.com/company/team/#jackib) | Product Designer: [Kevin Comoli](https://about.gitlab.com/company/team/#kcomoli) | Full Stack Engineer: [Doug Stull](https://about.gitlab.com/company/team/#dougstull) | Full Stack Engineer: Jackie Fraser

**Expansion Team Mission**

*   Expand ARR for current customers by driving seat expansion, upgrades, add ons, reduced discounts, etc.

**Expansion KPI**

*   [Net retention](https://about.gitlab.com/handbook/customer-success/vision/#financial-kpis)

_Supporting performance indicators:_

*   (#) of seats added 
*   % of seats added
*   Seats by stage 
*   Seats added by tier
*   Session level Usage (ping data)
*   (#) Seats by territory (where in the world)
*   (#) Seats by buyer Persona type ([buyer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas) persona)
*   User Persona type ([user](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#user-personas) persona)
*   Source of user acquisition (use channel e.g. paid, earned, social, owned)
*   Account usage
*   Seat utilization by account (how many seats did they sign up for and how many are they using)
*   % of instances (and groups, for GitLab.com) using more than ‘x’ stages 
*   MAU - For all stages - The number of unique users who had at least one session within a 28-day period. The 28-day period includes the last day in the active date range.
*   SMAU - Monthly active users by stage - this will be a subset of MAU at the stage level
*   SMAU Factor - expressed as the average number of stages used by users
*   Feature MAU - Monthly active users by feature - this will be a subset of SMAU
*   Feature MAU Factor - expressed as the average number of features by SMAU
*   Usage Pings - How many ping counts were created by all users
*   User Count Free - How many users were in the 'Free' plan
*   User Count Bronze - How many users were in the 'Bronze' plan
*   User Count Silver - How many users were in the 'Silver' plan
*   User Count Gold - How many users were in the 'Gold' plan
*   Total User Count - How many total users who have an account with GitLab

### Problems to solve

Do you have issues... We’d love to hear about them, how can we help GitLab contributors find that ah-Ha! Moment. 

Here are few problems we are trying to solve: 



*   **User Orientation** - Users don’t know where to start
    *   As a product manager, where do I start?
    *   What are the best tools for me to use on this platform as a security engineer? 
*   **Increase platform confidence and trust** - I love my tools and am afraid to switch
    *   We know our users love the tools they are currently using and that change is hard. We are trying to earn your trust and show you how to use our platform as one solution.
*   **Visibility** - Ever get that email, you know the one “can you give me an update on…” when individual contributors, management and executives are all on the same platform visibility increases 10 fold. 
    *   The more teams from your company using GitLab will dramatically improve visibility by reducing friction and noise and help you stay focused. 


### Our approach

Expansion runs a standard growth process. We gather feedback, analyze the information, ideate then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 


### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 


### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)

[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

Reports & Dashboards
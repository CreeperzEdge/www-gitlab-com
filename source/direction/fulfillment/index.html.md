---
layout: markdown_page
title: Product Vision - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

_"When technology delivers basic needs, user experience dominates."_  - Donald Norman

This is the product vision for GitLab's Fulfillment group. Below are the categories Fulfillment currently owns and improves.

If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Luca Williams via [e-mail](mailto:luca@gitlab.com),
[Twitter](https://twitter.com/tipyn2903), or by [scheduling a video call](https://calendly.com/tipyn).

| Category | Description |
| ------ |  ------ |
| [Account Management](/direction/fulfillment/account-management) | How our users purchase GitLab Plans and add-ons, and how they manage their account information and licenses. |
| [Trials](/direction/fulfillment/trials) | How our users sign up for and try GitLab. |

## ⛵️ Overview

The overall vision for Fulfillment is to provide GitLab’s customers with delightful trying, buying and onboarding experiences with GitLab. Additionally, Fulfillment strives to provide GitLab team members with great internal tools so that they may efficiently support and serve our users.

Billing, account management and license keys can be confusing, boring and time-consuming. We want these things to be easy and frictionless so you can focus on enjoying our product. In addition to our customers, we also prioritise the experience of GitLab Team Members (and Resellers). In order for our GitLab Team Members and Resellers to provide a great service to our customers, the administration and 'behind-the-scenes' user experience of our billing and licensing services are held to the same standard as the rest of our product.

It can be overwhelming when experiencing a new product for the first time, so Fulfillment aims to make this experience as engaging and as clear as possible. Trialling any software should be exciting as well as easy to set up.

Due to the fact that the Fulfillment group is responsible for collecting and storing personal and billing information, we also need to ensure that GitLab's services meet all compliance, security and privacy requirements.

### 🚀 Current Priorities

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [An improved free trial sign-up experience for GitLab.com (SaaS) users](https://gitlab.com/gitlab-org/gitlab-ee/issues/13233) | A free trial is often the first interaction our users have with our Product and therefore could be a dealbreaker for them. Currently, the free trial experience as a whole is confusing,lengthy and frustrating and this issue to improve the sign-up flow of our GitLab.com free trial is our first iteration towards creating a delightful and exciting first-look experience into GitLab as a product. |
| 2️⃣ | [License Sync - Cloud Based Licensing](https://gitlab.com/groups/gitlab-org/-/epics/1735)| We want to be able to provide the best service to our customers as possible. This feature reduces significant manual work on our sales and support teams, allows our security teams to protect you from vulnerabilities and your user management and billing experience is vastly improved, reducing friction and frustration for your system and billing administrators. Enabling automatic upgrades also removes the potential overhead for your sysadmins. This feature allows GitLab and our customers to collaborate more closely and opens up possibilities in the future for other exciting features our customers may ask for. Additionally, this protects GitLab from trial and license key abuse, meaning we spend less time handling those issues and more time helping you. |
| 3️⃣ | [An improved purchase experience for GitLab.com (SaaS) users](https://gitlab.com/gitlab-org/gitlab-ce/issues/63600) | Similarly to the free trial experience, our purchasing flow needs a serious refresh and this issue aims to significantly reduce the friction and frustration for our GitLab.com users who need to do business with us. |
| 4️⃣ | [Automatically bill for newly added group members on GitLab.com](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/168) | One of our goals in Fulfillment is to create a fair billing experience for our users and this issue is an iteration towards that goal. By implementing this, we can begin to shape a multi-directional billing flow that ensures our customers are fairly billed for what they use, and GitLab as a company accurately receives payment for the services provided. | 

 
## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

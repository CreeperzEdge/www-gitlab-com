---
layout: markdown_page
title: "Category Vision - Requirements Management"
---

- TOC
{:toc}

## Requirements Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

Requirements Management primer-type [article from PMI](https://www.pmi.org/learning/library/requirements-management-planning-for-success-9669)

Many organizations create applications with rigorous requirements that need to be
accurately tracked, and accounted for before developing the software, during, and
afterward. Larger requirements need to be further broken down into smaller ones,
with the relationships closely documented. This also ensures effective change control.
That is, if a requirement changes, the team should be able to see the downstream
impact immediately. Requirements management in GitLab addresses these problems 
and use cases.

See [issues for requirements management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management).

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The first step of this category is building out the initial requirements structure, in particular, at the group level. So that you can have a basic tree-structure of requirements. Future iterations include integration/linking with other places in GitLab, with test cases and issues, to achieve traceability. See https://gitlab.com/groups/gitlab-org/-/epics/707, the MVC of requirements management.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Top competitors in this area are traditional tools that do requirements management used by business analysts, managers, and similar personas. Jama Connect and IBM Rational DOORS are two popular tools. How GitLab can set ourselves apart is leveraging other parts of GitLab to directly integrate with requirements. This can be test cases, issues, and our pipelines.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The first step is building out the requirements management structure, scoped [here](https://gitlab.com/groups/gitlab-org/-/epics/707)

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Similar to above, we need to figure out the requirements structure - see epic [here](https://gitlab.com/groups/gitlab-org/-/epics/707)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Currently, GitLab team-members are not considering using Requirements Management.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
- MVC: https://gitlab.com/groups/gitlab-org/-/epics/707

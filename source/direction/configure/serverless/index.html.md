---
layout: markdown_page
title: "Category Vision - Serverless"
---

- TOC
{:toc}

## Serverless

Serverless computing provides an easy way to build highly scalable applications and services, eliminating the pains of provisioning & maintaining.

Today it's mostly used one-off for e.g. image transformations and ETL, but given the potential and the rise of microservices, it's fully possible to build complex, complete applications on nothing but serverless functions and connected services.

Leveraging knative and kubernetes, users will be able to define and manage functions in GitLab. This includes security, logging, scaling, costs, of their serverless implementation for a particular project/group.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Serverless)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/155) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

[Automatic domain for a serverless function](https://gitlab.com/gitlab-org/gitlab/issues/30151)

We want to make it easy to get started with GitLab serverless. Having to purchase and configure a domain to get started means a high bar for getting started. GitLab will automatically provision one for you.

## Competitive landscape

### AWS Lambda

AWS Lambda is a serverless compute service created by Amazon in 2015. It runs a function triggered by an event and manages the compute resources automatically so you don’t have to worry about what is happening under the hood.

### Azure Functions

Azure Functions is Microsoft’s response to Amazon’s Lambda. It offers a very similar product for the same cost. It uses Azure Web Jobs; the delay between hot cold invocations is less visible.

### Google Cloud Functions

It’s a fully managed nodeJS environment that will run your code handling scaling, security and performance. It’s event-driven and will trigger a function returning an event, very much in the same way AWS Lambda works. It’s intended to be used for small units of code that are placed under heavy load.

### Serverless Framework

The Serverless Framework is an open-source tool for managing and deploying serverless functions. It supports multiple programming languages and cloud providers. Its two main components are 1) [Event Gateway](https://serverless.com/event-gateway/), which provides an abstraction layer to easily design complex serverless applications, and 2)[Serverless Dashboard](https://serverless.com/dashboard/), for a better management of the application, as well as collaboration. Serverless Framework applications are written as YAML files (known as serverless.yml) which describe the functions, triggers, permissions, resources, and various plugins of the serverless application.

## Analyst landscape

The Serverless category is currently coupled with IaaS reports.

Gartner's `Magic Quadrant for Cloud Infrastructure as a Service` places AWS, Azure, and Google Cloud as leaders.

Forrester places `Serverless Computing` in their `Emerging Technology Spotlight` category, with the big three as leaders (AWS, Azure, Google Cloud)

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

[Automatic domain for a serverless function](https://gitlab.com/gitlab-org/gitlab/issues/30151)
[SSL for Knative services](https://gitlab.com/gitlab-org/gitlab-ce/issues/56467)

## Top internal customer issue(s)

TBD

## Top Vision Item(s) 

[SSL for Knative services](https://gitlab.com/gitlab-org/gitlab-ce/issues/56467)

## Related

[Serverless Monitoring](https://gitlab.com/groups/gitlab-org/-/epics/502)

---
layout: markdown_page
title: "Category Vision - Web Performance"
---

- TOC
{:toc}

## Web Performance

Ensure performance of your software in-browser using automated web performance testing.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20Performance)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1880) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Up next for Web Performance is updating to the latest version of the SiteSpeed Plugin via [gitlab-ee#10039](https://gitlab.com/gitlab-org/gitlab-ee/issues/10039). This will help us ensure we're providing the latest and greatest features.

## Maturity Plan

TBD

## Competitive Landscape

TBD

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD


---
layout: markdown_page
title: "IAM.2.01 - Unique Identifiers Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#  IAM.2.01 - Unique Identifiers

## Control Statement
GitLab requires unique identifiers for user accounts and prevents identifier reuse.

## Context
An unique identifier allows for every user's action to be logically separated and uniquely identified. It helps use detected and prevent abuse and suspicious activity.

## Scope

An unique identifier (UID) is a numeric or alphanumeric string that is associated with a single entity within a given system. UIDs make it possible to address that entity, so that it can be accessed and interacted with

## Ownership
* Control Owner: `IT Operations Team`
* Process owner(s):
    * IT Opeartion Team: `50%`
    * Security Team: `25%`
    * Security Compliance Team: `25%`


## Guidance
*  An unique security access identifier is most often the only basis for user authentication and authorization. Hence it is critical that every user has his or her own unique user ID. 
*  No two users should be allowed to share or be assigned the same user ID. If the uniqueness of user IDs is not assured, some users may be able to access information that they have not been given permission to access.
*  To ensure this, GitLab should have Policy and Guideline documentation that poses strict mandates that all GitLab accounts must use unique identifiers.
*  One mitigating factor can be a 100% implementation of Okta on all Gitlab applications, might keep a check on people using shared accounts, and promote them to use unique identifier at all times.

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Unique Identifiers control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/812).

## Framework Mapping

* ISO
  * A.9.4.1
  * A.9.4.2
* SOC2 CC
  * CC6.1
* PCI
  * 8.1.1
  * 8.6

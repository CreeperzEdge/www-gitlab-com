---
layout: markdown_page
title: "Commissions"
---


---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Quotas Overview

The quota coverage types are:
1. **Native Quota Req (NQR)** - individual owns direct quota for their assigned territory.
2. **Overlay Quota Rep (OQR)** - individual supports an assigned territory that is currently owned by a NQR.

Quotas may be adjusted based on geographic region and other factors.

### Quota Components

The following components are typical for most comp plans:
1. IACV: regional, territory, segment, or industry based
2. Professional Services: for native-quota carrying reps, the annual target is $50,000. For overlay quota reps, the target can be based on the reps who you support or roll up to you.
3. Professional Services Margins: this is typical for Implementation Engineers

For a list of the current comp plans, please review this Google Slides [presentation](https://docs.google.com/presentation/d/1S0X4AOq6DdP63T9AWDBhzPLuHU6jHJMlB4YHzMxhfjA/edit#slide=id.g33c9f07e39_1_37)

### Sales Rep IACV Quota Ramp and Seasonality Assumptions

For all native-quota carrying salespeople, we assume around an eleven (11) month ramp before a rep is fully productive.  We have the following ramp up schedule and measure performance based on this schedule:

* First Quarter Hired: 0% contribution of quarterly target
* Second Quarter Hired: 30% contribution of quarterly target
* Third Quarter Hired: 65% contribution of quarterly target
* Fourth Quarter Hired: 90% contribution of quarterly target

We also factor in seasonality into our calculations. We expect most of our business to close in the second half of the year. Our seasonality assumptions are as follows for the private sector:

* First Fiscal Quarter: 17%
* Second Fiscal Quarter: 23%
* Third Fiscal Quarter: 28%
* Fourth Fiscal Quarter: 32%

Given the fiscal period for public sector, our seasonality assumptions differ:

* First Fiscal Quarter: 15%
* Second Fiscal Quarter: 20%
* Third Fiscal Quarter: 55%
* Fourth Fiscal Quarter: 10%

In order to establish the quota for a given representative, the fully ramped quota and the start date are required. Once obtained, Sales Operations can proivde a prorated quota verbally to the Area Sales Manager, Regional Director, or Vice President, then deliver a Participant Schedule to the representative for acknowledgement of their plan and quota.

### Quotas Proration
1. Native Quota-Carrying Reps: prorated quotas are based on the seasonality assumptions as demonstrated above.
2. Overlay Quota Reps: proration is based off the number of days employed in the given period. Your prorated quota would be calculated as follows: (Quota / (Period End Date - Period Start Date)) * (Period End Date - Hire Date). For example, if the regional quota is $10,000,000 for the year and the start date is 4/1/2019, your quota will be calculated as ($10,000,000 / (2020-01-31 - 2019-02-01)) * (2020-01-31 - 2019-04-01) = $8,379,120.88.

## Commissions Overview
Most plans are paid monthly; however there are some plans that are paid on a quarterly basis. Most commissions plans are calculated in Xactly. 

### Commission Rates
You can find the definitions of **Base Commission Rate (BCR)** and **Super Commission Rates (SCR)** in the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/#definitions) page under the Definitions section.

### Commissions Rules
The following rules will apply for the various roles within the organization:

| **Role** | **Rules**  | **Explanation** |
| ------ | ------ | ------ |
| Strategic Account Leader, <br>Mid Market Account Executive | Is the Opportunity Owner | Opportunity owner will receive the commission |
| SMB Customer Advocate | Sales Segment is SMB or Unknown | The Ultimate Parent Segment is either SMB |
| Area Sales Manager | Opportunity Owner is subordinate AND<br>Account Owner is on Regional Team | If the Opportunity owner is a subordinate AND the Account Owner is on the ASM's regional team, the ASM will receive credit |
| Regional Director | Account Owner is your subordinate | The opportunity owner may be on a different regional team, but if the Account Owner is on the RD's team, the RD will receive credit. It is possible to override if the RD of the opportunity owner requests credit for the deal |
| Vice President (Sales) | Account Owner is your subordinate | The opportunity owner may be on a different Regional team, but if the Account Owner is on the VP's team, the VP will receive credit |
| Inside Sales Representatives (Public Sector) | Sub-Industry | The opportunity owner may be anyone on the PubSec team so long as the sub-industry is the one supported by the ISR |
| Solutions Architects | Opportunity Owner is on your regional team | The opportunity owner must be on the SA's team for them to receive credit |
| Technical Account Manager | Opportunity Owner is on your regional team | The opportunity owner must be on the TAM's team for them to receive credit |

### Opportunity Splits
Coming Soon

## Participant Schedules
Within the first few weeks of your employment, you should receive your participant schedule. This document will provide a high level overview of your plan components, territory, plan start and end dates, base and super (if applicable) commission rates, prorated quotas, and variable payouts per component.



---
layout: markdown_page
title: "SDR Enablement"
---

# SDR Coaching
In order to provide frequent feedback and coaching to the SDR team, weekly coaching/enablement sessions are hosted where the SDR team is able to ask questions about specific topics they are encountering with their customers. These coaching sessions are driven based on demand from the team and are interactive to help grow skills, awareness and knowledge about the IT and software delivery challenges that enterprise customers are facing.

## SDR Coaching Topics

[SDR Topic Planning Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/918054?&label_name[]=SDR-Coaching)

| Date | Topic | Recording |  
| :----: | :-----: | :---------: |
|  10/3/18  | [Enterprise IT Challenges](https://gitlab.com/gitlab-com/marketing/general/issues/3102) | [recording](https://drive.google.com/open?id=1O_U1xGp8N3eK4bLKWTQe4idffCkcv_fk) |
|  10/10/18 | [IT Roles CxO & VP (part1)](https://gitlab.com/gitlab-com/marketing/general/issues/3196)  | [recording](https://drive.google.com/open?id=1QLrf-Q6t6TBO2l9-8mPv5L1X9Q3Rf8bF)  |
|  10/17/18 | [IT Roles Directors & Mgrs (part2)](https://gitlab.com/gitlab-com/marketing/general/issues/3314)  | [recording](https://drive.google.com/open?id=1eXQU6YbP5yP7xeZsc0HNiDBQMsYDtX97) |
|  10/24/18 | [Qualification Questions: App Dev manager/director](https://gitlab.com/gitlab-com/marketing/general/issues/3413) | [recording](https://drive.google.com/open?id=1A3M5_CVQQORa2TotJ_IOEjW1J6Ddd_fI) |
|  10/31/18 | [Lead Qualification: role play](https://gitlab.com/gitlab-com/marketing/general/issues/3415)  | [recording](https://drive.google.com/open?id=1QXbjm_8i189DpCiK-wJSJ17GudsmVVeX)  |
|  11/7/18  | [Developer's life pre-gitlab](https://gitlab.com/gitlab-com/marketing/general/issues/3263) | [recording](https://drive.google.com/open?id=1KbegDeHX5vFv3_jlwFDLB0uB-FxNnNEO) |
|  11/14/18 | [Chorus training](https://gitlab.com/gitlab-com/marketing/general/issues/3565)  |  [recording]  |
|  11/28/18 | [LinkedIn Profile Interpretation](https://gitlab.com/gitlab-com/marketing/general/issues/3564)  | [recording] |
| 02/06/19 | [CI/CD Primer](https://about.gitlab.com/handbook/marketing/product-marketing/competitive/cicd/) | [recording] |


[All Recordings](https://drive.google.com/drive/u/0/folders/1h9tgvKOAMV0SSl2vlcfWL462YOsFop9L)

## Resources and useful links:

### IT Leaders, roles and their challenges
* [10 digital transformation success stories](https://www.cio.com/article/3149977/digital-transformation/digital-transformation-examples.html?nsdr=true#tk.cio_rs)
* [The CIO’s Dilemma: Innovate AND Cut Costs](https://www.cio.com/article/3300871/cloud-computing/the-cio-s-dilemma-innovate-and-cut-costs.html)
* [Complexity a killer when it comes to digital transformation success](https://www.cio.com/article/3269493/digital-transformation/complexity-a-killer-when-it-comes-to-digital-transformation-success.html)
* [5 Big Challenges CIOs face](https://www.mrc-productivity.com/blog/2017/11/5-big-challenges-facing-cios-leaders-2018/)
* [9 forces shaping the future of IT](https://www.cio.com/article/3206770/it-strategy/9-forces-shaping-the-future-of-it.html?upd=1538513299753)
* [12 biggest issues IT faces](https://www.cio.com/article/3245772/it-strategy/the-12-biggest-issues-it-faces-today.html)
* [Survey: Compliance Drives IT Security](https://www.cio.com/article/2447696/compliance/survey--compliance-drives-it-security.html)
* [Financial Services Regulatory Compliance](https://about.gitlab.com/solutions/financial-services-regulatory-compliance/)
* [Collaboration key to achieving business goals](https://www.cio.com/article/3170784/collaboration/collaboration-key-to-achieving-business-goals.html)
* [IT Job Roles](https://targetpostgrad.com/subjects/computer-science-and-it/it-job-roles-and-responsibilities-explained)
* [IT OrgCharts and Roles](http://www.bmcsoftware.in/guides/itil-itsm-roles-responsibilities.html)
* [CIO Role](https://www.thebalancecareers.com/business-or-it-what-s-the-main-job-of-a-cio-2071252)
* [Enterprise IT Roles](../enterprise-it-roles)

### Qualification
* [16 Sales Qualification Questions](https://blog.hubspot.com/sales/16-sales-qualification-questions-to-identify-prospects-worth-pursuing)
* [42 B2B qualifying questions](https://blog.close.io/42-b2b-qualifying-questions-to-ask-sales-prospects)

### Developers and DevOps
* [Git learning curve](/2017/05/17/learning-curve-is-the-biggest-challenge-developers-face-with-git/)
* [Continuous Integration with a rubber chicken](https://www.jamesshore.com/Blog/Continuous-Integration-on-a-Dollar-a-Day.html) - seriously, how to set up CI for only $10 with a rubber chicken and a bell
* [Continuous Integration isn't a tool](https://www.jamesshore.com/Blog/Continuous-Integration-is-an-Attitude.html)
* [Git vs SVN](https://stackoverflow.com/questions/871/why-is-git-better-than-subversion)
* [Continuous Integration - Martin Fowler](https://www.martinfowler.com/articles/continuousIntegration.html)

## Establishing the SDR Coaching program
### First meeting - AMA to set the stage and get organized

**Objective:**
* Encourage the team open up and be comfortable asking questions
* Establish ongoing agenda and get inputs for specific topics

### Proposed Recurring Agenda:
* Pre-work (preparation - Specific resource to read, objectives, questions)
* 10 min - Specific discussion topic  (PMM presents a topic)
* 19 min - Interactive / Q&A - (where SDRs share case study/situations)
* 1 min -  Next topic (prework/prep for next session)

### Requesting future discussion topics:
To request discussion topics and/or to see future topics see the [SDR Coaching issue board](https://gitlab.com/gitlab-com/marketing/product-marketing/boards/918054?&label_name[]=SDR-Coaching)

### Potential Resources:
* [An overview of IT/SW delivery challenges](https://learn.techbeacon.com/tracks/business-leaders-guide-software-innovation)
* Others TBD

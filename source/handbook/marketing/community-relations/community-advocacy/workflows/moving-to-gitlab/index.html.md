---
layout: markdown_page
title: "#movingtogitlab Workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

This is our movingtogitlab [Twitter account] https://twitter.com/movingtogitlab and the hashtag #movingtogitlab. The account is controlled via Tweetdeck. You can request access via #content Slack channel. 

We thank people for moving to GitLab and retweet them - avoid retweeting poisonous or explicitly worded tweets.  Don't forget to ask why they have moved or what's their favorite feature, we'd like to hear some feedback about our features ([example](https://twitter.com/damongentry/status/1136623462556979200)).  We also answer questions. The plan is to have a retweet/like list of all the tweets of people moving. 

## Workflow

For every tweet that we want to broadcast:
1. Like it

1. Retweet without comment

1. Reply with something like the below (feel free to use emojis! :boom: :tada: :wave: :rocket:   etc)
    * Thanks for #movingtogitlab! 
    * Thanks for considering GitLab! #movingtogitlab  
   * Thanks for #movingtogitlab early!  
   * We’re so glad to hear you’re #movingtogitlab! 
   * We share the same enthusiasm about #movingtogitlab!
   * Thanks for deciding to move to GitLab.  If you need assistance regarding #movingtogitlab, please reach out to us!
   * Woohoo! Thanks for #movingtogitlab  
   * You’ve made our day, thanks for #movingtogitlab : 
   * That’s what we like to hear!! Thanks for #movingtogitlab   
   * Yay! Thanks for sharing your #movingtogitlab story! 
   * Great to hear! We love seeing all these #movingtogitlab stories  
   * It’s so exciting to see people #movingtogitlab! We think we’re up to the challenge to accommodate everyone. 
   * We think we’re up to the challenge, but your supportive thoughts & vibes are very welcome!  #movingtogitlab
   * This is what we train for. Thanks for #movingtogitlab! We love seeing all the excitement. 
   * Well, nice to meet you!  Thanks for #movingtogitlab
   * How do you do?  Thanks for sharing your #movingtogitlab story! 
   * Howdy!  Great to meet all these new friends, thanks for #movingtogitlab
   * Welcome! Thanks for #movingtogitlab
   * Woohoo! Let us know if we can help in the transition.  Thanks for sharing your story about #movingtogitlab!
   * Awesome! Can’t wait to hear what you think. Let us know how we can help!  Thanks for #movingtogitlab
   * You made our day! Thanks for #movingtoGitlab :sparkles:
   * Let us know how we can help with #movingtogitlab !  And check out [https://about.gitlab.com/moving-to-gitlab/](https://about.gitlab.com/moving-to-gitlab/)
   * Welcome aboard!  Thanks for #movingtoGitlab 
   * (waving tanuki gif) [https://media.giphy.com/media/xUA7aMGO1T5ddNno0E/giphy.gif](https://media.giphy.com/media/xUA7aMGO1T5ddNno0E/giphy.gif)
1. Sunday specific tweets
    * This is the relaxing Sunday we had planned, why do you ask? :laughing:   Thanks for #movingtogitlab! 
1. People having problems with migrations or other issues: [https://gitlab.com/gitlab-com/support-forum/issues](https://gitlab.com/gitlab-com/support-forum/issues)



If the tweet is in another language, open in Twitter web interface, press translate. If it looks legitimate then retweet and like, and either uses Google Translate to thank them in their local language, or ask for help from a GitLab team-member who speaks that language.


There is no need to document the tweets other than liking and retweeting them.


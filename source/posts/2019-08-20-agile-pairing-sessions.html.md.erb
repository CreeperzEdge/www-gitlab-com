---
title: "Agile for developers: pairing sessions"
author: Suri Patel
author_gitlab: suripatel
author_twitter: gitlab
categories: insights
image_title: '/images/blogimages/incrementalcodedevelopment.jpg'
description: "Pairing with a teammate can increase delivery. Here's a look at the pros and cons."
tags: agile, collaboration, DevOps, workflow
postType: content marketing
cta_button_text: 'Join us at our first user conferences in Brooklyn and London!'
cta_button_link: '/events/commit/'
---

Arya and Sansa. Han and Chewbacca. Harry and Ron. When people team up, great things can happen.

Pair programming, an Agile approach to software development, involves two
programmers working together at the same workstation. One programmer (called the driver)
writes code while the other programmer (called the navigator) reviews code in real time.
Pairing sessions can accelerate Agile delivery, because teammates work together to find
the best solutions to several challenges. Rather than working in silos, team members
work together to share knowledge and quickly move through obstacles. Sounds good, right?
Well, some organizations view pair programming as an inefficient use of time. After all,
why should two developers work on the same piece of code when there’s a mountain of
technical debt, an impending release, and lingering OKRs around the corner?

## The case for and against pair programming

There are benefits and drawbacks to pairing sessions, so a few GitLab team members
shared their thoughts to help you determine whether pair programming is right for you.

> “I've done pair programming in the past. I love it because it helps to bounce
ideas off people, and I find we often could solve ‘bigger’ problems faster. To me,
the downside is measuring/proving that this is a good method of programming since
many people see this as inefficient (two people working on the same problem).” –
[Cynthia Ng](/company/team/#TheRealArty), senior support agent

Today’s developer feels the pressure of delivering at rapid speeds. Sometimes, a
challenge is just too complex for one person to solve, and pairing sessions can
help alleviate the difficulties experienced when racing towards a release while
carrying a burdensome issue. Talking through solutions and drawing on each other’s
experiences can help a pair work towards a new approach.

Measuring the effectiveness of pairing sessions might be difficult, but there are ways to
evaluate success. Considering failures in functionality, the number of
bugs, and improvements in productivity can help teams determine whether pairing
makes a difference with delivery.

### The role of engagement and continuous learning in delivery

IT leaders may be reluctant to embrace pairing, since two developers dedicate
their time to a single problem, but it’s important to note researchers have
found that
[90% of new skills learned are lost due to lack of
engagement](https://www.wsj.com/articles/SB10001424052970204425904578072950518558328),
and in an Agile framework, a culture of continuous learning helps improve all aspects of delivery.

> “When I was a junior developer, I found it very helpful to talk through my
thought process and hear how senior developers approached the same problem. But,
as an introvert, I found it exhausting to do all day, every day.” –
[Jennie Louie](/company/team/#jennielouie), test automation engineer, Enablement

Agile models often include the value of continuous learning to help everyone –
from C-level to junior level – develop new skills to remain adaptable and productive.
Pairing sessions provide a platform from which teammates can learn in tandem.

> “I’ve never done ‘strict’ pairing with a driver/navigator, only the relaxed kind
where you just chat and sometimes switch keyboards. And while I can't really imagine
pairing full-time, I guess with the right pair and some practice it could indeed be
a great experience.” – [Markus Koller](/company/team/#toupeira), backend engineer, Create:Editor

The drawbacks to pair programming might make you hesitate, but I encourage you to
take a chance on it, especially if you want to accelerate delivery. Here are a
few pros and cons of pairing to help you understand the process:

### Advantages of pair programming

Directly collaborating with a teammate can increase morale and inject fun and
diversity in one’s day. By working alongside each other, teammates can learn
different coding practices, workflow techniques, and new ways of approaching
problems, which increases innovation and efficiency and decreases knowledge silos.

> “Pair programming can be great for onboarding, mentoring, and [rubber ducking](https://en.wikipedia.org/wiki/Rubber_duck_debugging)
difficult problems, since teammates receive immediate
feedback.” – [Andrew Kelly](https://gitlab.com/ankelly), senior security engineer, Application Security

Junior developers benefit when pair programming with senior developers, since they’ll
gain strong industry knowledge. Meanwhile, senior developers get teaching experience
and the ability to think critically about solutions.

<%= partial "includes/blog/content-newsletter-cta", locals: { variant: "b" } %>

> “Programming is fairly abstract. When you have to explain a concept verbally, it
often makes you realize you're missing pieces or that there are better
ways to solve problems than your initial idea.” – [Brandon Lyon](/company/team/#brandon_m_lyon), marketing web developer/designer

Regardless of experience level, everyone can benefit from pairing sessions, since
there is no right answer in programming. I consider software development a multi-faceted
endeavor in which imagination and creativity are driving forces. Based on knowledge,
experience, and learning styles, people approach some aspects of code with
a different understanding of how it ties into existing systems. When pairing, people can
discuss these perspectives and assess which approach is best.

### Disadvantages of pair programming

Pairing might sound like the solution to many of your delivery problems, but it’s
not all roses and rainbows.

Given the success of pairing, teammates might be tempted to join forces a little
too often. Pair programming can feel inefficient if overdone or used for tasks
such as boilerplate code, smaller and well-defined changes, and [yak shaving](https://www.techopedia.com/definition/15511/yak-shaving).

> “Pair programming is not a silver bullet. Some software solutions just need a
single person to hunker down and work it out before sharing with others.” – [Andrew Kelly](https://gitlab.com/ankelly)

If teams are just starting out with pairing, it can take practice and patience
to be a “good pair,” which can be difficult even for experienced pair programmers.
Do retros after a pairing session to understand what worked well, what didn’t work,
and how you can improve future sessions.

## See it in action

Now that you know a bit more about pair programming, you might feel ready to take
the plunge. At GitLab, we 💖 pairing. Most pairing sessions occur when developers
work at the same station, but as an [all-remote company](/company/culture/all-remote),
we’ve found ways to make it work.

> “Remote pair programming can be tougher than in-person pairing. Distance plus the
tooling isn’t always the best, but it’s not impossible.” – [Andrew Kelly](https://gitlab.com/ankelly)

GitLab’s Support team created a [dedicated project and issue templates for pairing
sessions](https://gitlab.com/gitlab-com/support/support-training/issues?label_name%5B%5D=pairing).

> “In Support, we do pairing sessions (or group ‘crush sessions’) and find we often
get through _more_ tickets when working together, so it's something we're tracking
as a milestone for each quarter.” – [Cynthia Ng](/company/team/#TheRealArty)

Over in engineering, the Frontend team has also been [experimenting with how to support
pair programming](https://gitlab.com/gitlab-org/frontend/general/issues/12). The
team has used VSCode live share a few times but enjoys open discussion and sending
patches to each other.

> “The best format so far is someone posts a "🍐 request" in the #frontend_pairs
Slack channel – people show interest – a time is scheduled on the calendar – then
we do somewhat of a mob programming session.” – [Paul Slaughter](/company/team/#pslaughter), frontend engineer, Create:Editor

Every software team hears the importance of acceleration, and it can be a daunting
thought, especially when faced with complex problems. The next time you find
yourself dragging your fingers across the keyboard and dreading that next line of
code, consider pairing up with a teammate to tackle issues together.

> “Pairing will look different for everyone. Anything that encourages
communication, engaged knowledge sharing, and breaking our engineering silos is
good.” – [Paul Slaughter](/company/team/#pslaughter)

Cover image by [Jonathan Mast](https://unsplash.com/@jonathanmast) on [Unsplash](https://unsplash.com/photos/RW6Wz9QaoKk)
{: .note}
